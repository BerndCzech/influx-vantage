#!/usr/bin/env sh
name="influx"
openssl req -x509 -out ${name}.crt -keyout ${name}.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj "/CN=${name}" -extensions EXT -config <( \
   printf "[dn]\nCN=${name}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${name}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")