// Package db handles all data exchange
// to the db.
package db

import (
	"bytes"
	"context"
	"crypto/tls"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/influxdata/influxdb-client-go/v2"
	"github.com/pkg/errors"

	// "strconv"
	// underscore is necessary
	// for the ps driver to work
	_ "github.com/lib/pq"
)

type Config struct {
	PGUser            string        `envconfig:"POSTGRES_USER"`
	PGPW              string        `envconfig:"POSTGRES_PASSWORD"`
	PGDB              string        `envconfig:"POSTGRES_DB"`
	AlphaRequestWait  time.Duration `envconfig:"ALPHA_REQUEST_WAIT"`
	AlphaFreeplanWait time.Duration `envconfig:"ALPHA_FREEPLAN_WAIT"`
	AlphaKey          string        `envconfig:"ALPHA_KEY"`
	FluxToken         string        `envconfig:"FLUX_TOKEN"`
	FluxEndpoint      string        `envconfig:"FLUX_ENDPOINT"`
	FluxORG           string        `envconfig:"FLUX_ORG"`
	FluxBucket        string        `envconfig:"FLUX_BUCKET"`
}

type app struct {
	*sql.DB
	FluxClnt influxdb2.Client
	Config
	Log *log.Logger
}

func NewApp(c Config, l *log.Logger) *app {
	p, err := sql.Open(
		"postgres",
		fmt.Sprintf("postgresql://%s:%s@postgrs:5432/%s?sslmode=disable", c.PGUser, c.PGPW, c.PGDB))
	if err != nil {
		return nil
	}

	o := influxdb2.DefaultOptions()
	o.SetTLSConfig(&tls.Config{InsecureSkipVerify: true})
	// increases write performance
	o.SetUseGZip(true)
	//  0 error, 1 - warning, 2 - info, 3 - debug
	o.SetLogLevel(1)
	o.SetPrecision(time.Second)
	client := influxdb2.NewClientWithOptions(c.FluxEndpoint, c.FluxToken, o)
	return &app{DB: p, Log: l, FluxClnt: client, Config: c}
}

func (a app) Close() error {
	a.FluxClnt.Close()
	return a.DB.Close()
}

// TemporaryError says that a instant rerun is sensible.
type TemporaryError struct {
	RetryValid bool
	Err        error
}

func (e *TemporaryError) Error() string {
	if !e.RetryValid {
		return fmt.Sprintf("Difficult (non-temporary) issue with data or alphaVantage: %s ", e.Err)
	}
	return fmt.Sprintf("Temporary issue with data or alphaVantage: %s ", e.Err)
}

type evalDate = string

//grouped type declaration for JSON doc.
type (
	dataTimeSeries struct {
		MetaInfo   metaData               `json:"Meta Data"`
		TimeSeries map[evalDate]singleDay `json:"Time Series (Daily)"`
	}

	metaData struct {
		Information string `json:"1. Information"`
		Symbol      string `json:"2. Symbol"`
		LastRefresh string `json:"3. Last Refreshed"`
		OutputSize  string `json:"4. Output Size"`
		TimeZone    string `json:"5. Time Zone"`
	}
	singleDay struct {
		Open             string `json:"1. open"`
		High             string `json:"2. high"`
		Low              string `json:"3. low"`
		Close            string `json:"4. close"`
		AdjustedClose    string `json:"5. adjusted close"`
		Volume           string `json:"6. volume"`
		DividendAmount   string `json:"7. dividend amount"`
		SplitCoefficient string `json:"8. split coefficient"`
	}
)

func writeErr(err error) error {
	return fmt.Errorf("fill: %v", err)
}

// Fill puts marketdata into the DB.
func (a app) Fill(ctx context.Context, ShareCodes ...string) error {
	// HTTP (here sql connection) servers are useful for demonstrating the usage of context.
	// Context for controlling cancellation. A Context carries deadlines,
	// cancellation signals, and other request-scoped values across API boundaries and goroutines.
	appSignal := make(chan os.Signal, 1)
	signal.Notify(appSignal, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	// Assure DB is available
	a.Ping(ctx)

	// read from database
	readData, err := a.getMarketdata(ctx, ShareCodes...)
	if err != nil {
		return &TemporaryError{RetryValid: true, Err: err}
	}

	r := bytes.NewReader(readData)

	stockDataRaw, err := readJSON(r)
	if err != nil {
		return writeErr(err)
	}

	stockDataStruct := dataTimeSeries{}
	if err := json.Unmarshal(stockDataRaw, &stockDataStruct); err != nil {
		return writeErr(err)
	}

	// Either wrong Symbol or free access limit reached
	if stockDataStruct.TimeSeries == nil {
		return nil
	}

	// update from database
	if err = a.edit(ctx, stockDataStruct); err != nil {
		return writeErr(err)
	}

	return nil
}

func readJSON(r io.Reader) ([]byte, error) {
	dat, err := ioutil.ReadAll(r)
	return dat, err
}

// getMarketdata gets data from alphaventage
// If ShareCodes is left empty a Symbol will be auto-picked
func (a *app) getMarketdata(ctx context.Context, ShareCodes ...string) ([]byte, error) {
	var err error
	var ShareCode string

	switch givenShareCodes := len(ShareCodes); {
	case givenShareCodes == 0 || givenShareCodes == 1 && ShareCodes[0] == "":
		ShareCode, err = a.queryNextShare(ctx)
		if err != nil {
			log.Print(err)
			return nil, errors.Errorf("GetMarketdata: Could not find next share")
		}
	case givenShareCodes == 1:
		ShareCode = ShareCodes[0]
	default:
		return nil, errors.Errorf("GetMarketdata: more then 1 Symbol is not yet supported")
	}

	var MyKey = a.Config.AlphaKey
	const APIURL = "https://www.alphavantage.co/query"

	client := &http.Client{}

	req, err := http.NewRequest("GET", APIURL, nil)
	if err != nil {
		a.Log.Fatalf("%v", err)
	}

	q := req.URL.Query()
	q.Add("function", "TIME_SERIES_DAILY_ADJUSTED")
	q.Add("symbol", ShareCode)
	q.Add("outputsize", "full")
	q.Add("datatype", "json")
	q.Add("apikey", MyKey)

	req.URL.RawQuery = q.Encode()

	var respBody []byte
	resp, err := client.Do(req)
	if err != nil {
		return respBody, fmt.Errorf("GetMarketdata: Errored when sending request to the server: %v", err)
	}

	defer resp.Body.Close()
	respBody, _ = ioutil.ReadAll(resp.Body)

	// TODO: create error for <> 200 like response body (check std. libs)
	a.Log.Println(resp.Status)
	if resp.StatusCode != http.StatusOK {
		return respBody, fmt.Errorf("GetMarketdata: Non 200 StatusCode: %d", resp.StatusCode)
	}

	switch apiAnswer := string(respBody); {
	case strings.Contains(apiAnswer, "Invalid API call"):
		err = a.markSymbolInvalid(ctx, ShareCode)
		return nil, err
	case strings.Contains(apiAnswer, "500 calls per day"):

		yyyy, mm, dd := time.Now().UTC().Date()
		nextDay := time.Date(yyyy, mm, dd+1, 0, 0, 0+1, 0, time.UTC)

		a.Log.Printf("Waiting until %v so the free alphavantage usage is open again.", nextDay)
		ticker := time.NewTicker(a.Config.AlphaFreeplanWait)
		defer ticker.Stop()

		select {
		case <-ctx.Done():
			return nil, nil

		case <-ticker.C:
			if 0 < nextDay.Sub(time.Now().UTC()) {
				return a.getMarketdata(ctx, ShareCode)
			}
		}
	}

	return respBody, nil

}
