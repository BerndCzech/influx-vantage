# Defining default environment

FROM golang:1.17.3-alpine3.14 as prod-build
ARG CGO_ENABLED=0
# Build Delve
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM"
#RUN go get github.com/go-delve/delve/cmd/dlv

WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
ADD . .

RUN go build -gcflags="all=-N -l" -o influxvantage cmd/getassetdata.go


# Final stage
FROM debian:buster as prod-run
EXPOSE 8000 40000
RUN apt-get update && apt-get upgrade -y
WORKDIR /
COPY --from=prod-build /app/influxvantage /
# Copy CA certificates to prevent x509: certificate signed by unknown authority errors
COPY --from=prod-build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENTRYPOINT [ "./influxvantage" ]

