module gitlab.com/BerndCzech/influx-vantage

go 1.16

require (
	github.com/influxdata/influxdb-client-go/v2 v2.5.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.10.2
	github.com/pkg/errors v0.9.1
)
