package db

import (
	"context"
	"fmt"
	"strconv"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/pkg/errors"
)

// Query the database for the information requested and prints the results.
// If the query fails exit the program with an error.
// func queryNextShare(ctx context.Context, parameter int)
func (a *app) queryNextShare(ctx context.Context) (string, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	//err := a.QueryRowContext(ctx, "select p.name from people as p where p.id = :id;", sql.Named("id", id)).Scan(&name)
	rows, err := a.QueryContext(ctx, `
		SELECT symbol from staticdata.shares
		where isvalid=true
		order by  lastrefreshed asc,shareid
		limit 1`)
	if err != nil {
		return "", fmt.Errorf("connect: %w", err)
	}

	defer rows.Close()
	var resSymbol string
	for rows.Next() {

		err = rows.Scan(&resSymbol)
	}
	if err != nil {
		return "", fmt.Errorf("connect: %w", err)
	}
	return resSymbol, nil

}

func (a *app) edit(ctx context.Context, d dataTimeSeries) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	// language=PostgreSQL
	updateMarketData := `update staticdata.shares
		set lastrefreshed = cast(now() at time zone 'utc' as date)
		From staticdata.assettype
		where symbol = $1
		  and isvalid = true
		  and assettype.assettypeid = shares.assettypeid
		RETURNING shareid, sharename, assettype.assettype;`

	var (
		uuid      string
		shareName string
		assetType string
	)

	if err := a.QueryRow(updateMarketData, d.MetaInfo.Symbol).Scan(&uuid, &shareName, &assetType); err != nil {
		return errors.Wrap(err, "writing to influx failed")
	}
	tags := map[string]string{
		"symbol":     d.MetaInfo.Symbol,
		"time_zone":  d.MetaInfo.TimeZone,
		"share_id":   uuid,
		"share_name": shareName,
		"asset_type": assetType,
	}
	writeAPI := a.FluxClnt.WriteAPI(a.FluxORG, a.FluxBucket)
	errorsCh := writeAPI.Errors()
	go func() {
		for err := range errorsCh {
			a.Log.Printf("influx: %v\n", err)
		}
	}()

	for date, day := range d.TimeSeries {
		t, err := time.Parse("2006-01-02", date)
		if err != nil {
			return errors.Wrap(err, "writing to influx failed")
		}
		p := influxdb2.NewPoint("stock_prices", tags, toFday(day, d.MetaInfo.LastRefresh), t)
		writeAPI.WritePoint(p)
	}
	writeAPI.Flush()
	a.Log.Printf("Probably Inserted/Updated: %d rows for %s.", len(d.TimeSeries), d.MetaInfo.Symbol)
	return nil
}

func toFday(day singleDay, lastRefresh string) map[string]interface{} {

	c, _ := strconv.ParseFloat(day.Close, 64)
	ac, _ := strconv.ParseFloat(day.AdjustedClose, 64)
	h, _ := strconv.ParseFloat(day.High, 64)
	l, _ := strconv.ParseFloat(day.Low, 64)
	da, _ := strconv.ParseFloat(day.DividendAmount, 64)
	sc, _ := strconv.ParseFloat(day.SplitCoefficient, 64)
	v, _ := strconv.Atoi(day.Volume)
	o, _ := strconv.ParseFloat(day.Open, 64)

	fDay := map[string]interface{}{
		"close":            c,
		"AdjustedClose":    ac,
		"High":             h,
		"Low":              l,
		"DividendAmount":   da,
		"SplitCoefficient": sc,
		"Volume":           v,
		"Open":             o,
		"last_refreshed":   lastRefresh,
	}
	return fDay
}

// Ping the database to verify DSN provided by the user is valid and the
// server accessible. If the ping fails exit the program with an error.
func (a *app) Ping(ctx context.Context) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	countTrial := 1

	if err := a.PingContext(ctx); err != nil {
		countTrial++
		if countTrial > 5 {
			a.Log.Fatalf("%d Trials failed unable to connect to database: %s", countTrial, err)
		}
		time.Sleep(3 * time.Second)
	}

}

func (a *app) markSymbolInvalid(ctx context.Context, StockSymbol string) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	deactivateSymbol := `
	update staticdata.shares 
	set isvalid =false,lastrefreshed  = cast( now() at time zone 'utc' as date)
	where isvalid = true and symbol = $1`

	res, err := a.Exec(deactivateSymbol, StockSymbol)
	if err != nil {
		return fmt.Errorf("connect: %w", err)
	}
	nRows, _ := res.RowsAffected()

	a.Log.Printf("Deactivated %d shares with symbol: %s.", nRows, StockSymbol)
	return &TemporaryError{
		RetryValid: true,
		Err:        fmt.Errorf("Stock marked as invalid: %s", StockSymbol),
	}

}
