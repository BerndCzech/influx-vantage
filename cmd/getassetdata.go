package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/BerndCzech/influx-vantage/pkg"
)

func main() {
	var config db.Config
	err := readEnv(&config)
	if err != nil {
		log.Fatal(err)
	}

	err = run(config)
	if err != nil {
		log.Fatal(err)
	}

}

func run(c db.Config) error {
	// dataBase Stuff
	appSignal := make(chan os.Signal, 1)
	signal.Notify(appSignal, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		select {
		case a := <-appSignal:
			fmt.Printf("\ngot the signal to stop: %v \n", a)
			cancel()
			wg.Wait()
			os.Exit(1)
		}
	}()

	l := log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	d := db.NewApp(c, l)
	defer d.Close()

	d.SetConnMaxLifetime(0)
	d.SetMaxIdleConns(3)
	d.SetMaxOpenConns(3)

	for {
		wg.Add(1)
		err := d.Fill(ctx)
		if err != nil {
			switch e := err.(type) {
			case *db.TemporaryError:
				if e.RetryValid {
					break
				}
				l.Printf("Application Error: \n %v \n", err)
				os.Exit(1)
			default:
				l.Printf("Application Error: \n %v \n", err)
				os.Exit(1)
			}
		}
		// API call 5 times per minute
		wg.Done()
		time.Sleep(c.AlphaRequestWait)
	}
}

func readEnv(cfg *db.Config) error {
	return envconfig.Process("", cfg)
}
